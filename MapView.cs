﻿using Acrotech.LiteMaps.Engine;
using System.Windows.Forms;
using System;

namespace Acrotech.LiteMaps.WinForms
{
    public partial class MapView : UserControl
    {
        private MapViewModel propViewModel = null;

        public MapView()
        {
            InitializeComponent();

            Disposed += OnDisposed;

#if !PocketPC
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
#endif
        }

        public MapViewPort ViewPort { get; private set; }

        public MapViewModel ViewModel
        {
            get { return propViewModel; }
            set
            {
                propViewModel = value;

                if (propViewModel != null)
                {
                    ViewPort = new MapViewPort(Canvas, ViewModel.ViewPort);

                    ViewPort.Initialize();
                }
            }
        }

        public virtual void AddLayer(WinFormsLayer layer)
        {
            ViewPort.AddLayer(layer);

            ViewModel.Layers.Add(layer.ViewModel);
        }

        private void OnDisposed(object sender, EventArgs e)
        {
            ViewModel.Dispose(); 
            ViewPort.Dispose();
        }
    }

    public class MapCanvas : Control, IMapCanvas
    {
#if PocketPC
        public event MouseEventHandler MouseDoubleClick = null;

        private MouseEventArgs LastMouseDownEventArgs { get; set; }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            LastMouseDownEventArgs = e;
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);

            var mouseDoubleClick = MouseDoubleClick;

            if (mouseDoubleClick != null)
            {
                mouseDoubleClick(this, LastMouseDownEventArgs);
            }
        }
#endif

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            //base.OnPaintBackground(pevent);
        }
    }
}
