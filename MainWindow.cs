﻿using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.WinForms.Layers;
using System.Windows.Forms;
using DotSpatial.Positioning;
using Acrotech.LiteMaps.Engine.Sources;

namespace Acrotech.LiteMaps.WinForms
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

#if !PocketPC
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
#endif

            Map.ViewModel = ViewModel = new MapViewModel(new BingTileImageSource(BingTileType.Satellite, BingTileImageSource.DefaultVersion, BingTileImageSource.DefaultLanguage));

            Map.AddLayer(new FPSLayer(Map.ViewPort, LayerCornerPosition.TopLeft));
            Map.AddLayer(new TileDownloadStatusLayer(Map.ViewPort, LayerCornerPosition.BottomLeft));
            Map.AddLayer(new TargetMarkerLayer());
            Map.AddLayer(new GpsMarkerLayer());

            Gps = new GpsSimulator();
            ViewModel.TargetPosition = Gps.CurrentPosition;
            Gps.PositionChanged += (p, a) => { ViewModel.GpsPosition = p; ViewModel.GpsAccuracy = a; };
            Gps.Start();
        }

        private MapViewModel ViewModel { get; set; }
        private GpsSimulator Gps { get; set; }
    }
}
