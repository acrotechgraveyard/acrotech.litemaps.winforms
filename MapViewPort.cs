﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Acrotech.LiteMaps.Engine;
using Acrotech.PortableLogAdapter;
using Acrotech.PortableViewModel;
using System.Threading;
using Acrotech.LiteMaps.Engine.Storage;
using System.IO;
using Acrotech.LiteMaps.Engine.Sources;

#if !PocketPC
using System.Threading.Tasks;
#endif

namespace Acrotech.LiteMaps.WinForms
{
    public partial class MapViewPort : IDisposable
    {
        private static readonly ILogger Logger = 
#if PocketPC
            Services.LogManager
#else
            Program.DesignModeSafe(() => Program.LogManager
#endif
                .GetLogger<MapViewPort>()
#if PocketPC
            ;
#else
            );
#endif

        public static readonly Brush FrameWipeBrush = new SolidBrush(Color.Black);
        public static readonly Brush DebugTextBrush = new SolidBrush(Color.White);
#if !PocketPC
        public static readonly Brush DebugTextBackgroundBrush = new SolidBrush(Color.FromArgb(100, 100, 100, 100));
#endif

        public static readonly Pen GridPen = new Pen(Color.White, 1f);
        public static readonly Pen CrossPen = new Pen(Color.Gray, 1f);

        public const float DebugFontSize = 
#if PocketPC
            6f;
#else
            12f;
#endif

        public static readonly Font DebugFont = new Font("Courier New", DebugFontSize, FontStyle.Regular);

#if !PocketPC
        private static readonly TaskScheduler UITaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
#endif

        public MapViewPort(IMapCanvas canvas, ViewPortViewModel viewModel)
        {
            Canvas = canvas;
            ViewModel = viewModel;
        }

        public IMapCanvas Canvas { get; private set; }
        public ViewPortViewModel ViewModel { get; private set; }

        private Dictionary<string, Image> Images { get; set; }
        private Dictionary<Guid, WinFormsLayer> Layers { get; set; }

        private ManualResetEventSlim WaitHandle { get; set; }

        public Image Buffer { get; private set; }
        private Image BackBuffer { get; set; }
        private bool RefreshRequired { get; set; }

        private ITileStorage Storage { get { return ViewModel.Map.TileImageSource.Storage; } }

        public virtual void Initialize()
        {
#if PocketPC
            Canvas.Resize += OnSizeChanged;
#else
            Canvas.SizeChanged += OnSizeChanged;
#endif
            Canvas.Paint += OnPaint;
            Canvas.MouseDown += OnMouseDown;
            Canvas.MouseUp += OnMouseUp;
            Canvas.MouseMove += OnMouseMove;
            Canvas.MouseDoubleClick += OnDoubleClick;
#if !PocketPC
            Canvas.MouseWheel += OnMouseWheel;
#endif

            Images = new Dictionary<string, Image>();
            Layers = new Dictionary<Guid, WinFormsLayer>();

            WaitHandle = new ManualResetEventSlim(false);

            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
            TileImageSource.Downloader.TileDownloaded += (s, t) => RefreshMapAsync();

            ThreadPool.QueueUserWorkItem(_ => ImageLoader(ViewModel));
            ThreadPool.QueueUserWorkItem(_ => Refresher());

            OnSizeChanged(Canvas, EventArgs.Empty);
        }

        protected virtual void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Tiles")
            {
                RefreshMapAsync();
            }
        }

        public void RefreshMapAsync()
        {
            RefreshRequired = true;
        }

        public virtual void RefreshMap()
        {
            Canvas.Invalidate();

            RefreshRequired = false;
        }

        private void RefreshBufferImage()
        {
            var buffer = Buffer;
            var backBuffer = BackBuffer;

            if (buffer != null && backBuffer != null)
            {
                using (var g = Graphics.FromImage(backBuffer))
                {
                    g.FillRectangle(FrameWipeBrush, new Rectangle(0, 0, backBuffer.Width, backBuffer.Height));

                    var tiles = ViewModel.Tiles;

                    if (tiles != null)
                    {
                        var tileSize = ViewModel.Map.TileImageSource.TileSize;

                        Image image = null;

                        foreach (var tile in tiles)
                        {
                            var bounds = new Rectangle(tile.X, tile.Y, tileSize, tileSize);

                            if (Images.TryGetValue(tile.Tile.Key, out image))
                            {
                                g.DrawImage(image, bounds.X, bounds.Y);
                            }

                            if (image == null || ViewModel.IsTileGridVisible)
                            {
                                g.DrawRectangle(GridPen, bounds);
                            }

                            if (image == null || ViewModel.IsTileDebugTextVisible)
                            {
#if !PocketPC
                                g.FillRectangle(DebugTextBackgroundBrush, bounds);
#endif

                                var txt = image == null ? ViewModel.TileLoadingText : string.Empty;

                                if (ViewModel.IsTileDebugTextVisible)
                                {
                                    txt += Environment.NewLine + tile.ToString();
                                }

                                var txtBounds = g.MeasureString(txt, DebugFont
#if !PocketPC
                                    , tileSize
#endif
                                );

                                g.DrawString(txt, DebugFont, DebugTextBrush, bounds.X + tileSize / 2 - txtBounds.Width / 2, bounds.Y + tileSize / 2 - txtBounds.Height / 2);
                            }
                        }

                        foreach (var layer in ViewModel.Map.EnabledLayers.Select(x => GetLayer(x)).Where(x => x != null))
                        {
                            layer.OnRender(g, ViewModel, this);
                        }
                    }

                    if (ViewModel.IsCrossVisible)
                    {
                        var x = backBuffer.Width / 2;
                        var y = backBuffer.Height / 2;

                        g.DrawLine(CrossPen, x, 0, x, backBuffer.Height);
                        g.DrawLine(CrossPen, 0, y, backBuffer.Width, y);
                    }
                }

                lock (buffer)
                {
                    using (var g = Graphics.FromImage(buffer))
                    {
                        g.DrawImage(backBuffer, 0, 0);
                    }
                }
            }
        }

        private void OnSizeChanged(object sender, EventArgs e)
        {
            var size = Canvas.Bounds.Size;

            if (size.IsEmpty == false && ViewModel != null)
            {
                Buffer = new Bitmap(size.Width, size.Height);
                BackBuffer = new Bitmap(size.Width, size.Height);

                ViewModel.Resize(size.Width, size.Height);
            }
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            var buffer = Buffer;

            if (buffer != null)
            {
                lock (buffer)
                {
                    e.Graphics.DrawImage(buffer, Canvas.Bounds.X, Canvas.Bounds.Y);
                }
            }
        }

        protected virtual void Refresher()
        {
            const int MaxFPS = 60;
            const int Interval = 1000 / MaxFPS;

            Logger.Debug("Entering Map View Port Refresher Thread");

            try
            {
                while (WaitHandle.Wait(0) == false)
                {
                    try
                    {
                        if (RefreshRequired)
                        {
                            RefreshBufferImage();

#if PocketPC
                            Canvas.Invoke((Action)
#else
                            Task.Factory.StartNew(
#endif
                                RefreshMap
#if !PocketPC
                                , CancellationToken.None, TaskCreationOptions.None, UITaskScheduler
#endif
                            );
                        }
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                    WaitHandle.Wait(Interval);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorException(e, "Unexpected Error in Map View Port Refresher Thread");
            }

            Logger.Debug("Entering Map View Port Refresher Thread");
        }

        protected virtual void ImageLoader(ViewPortViewModel viewModel)
        {
            Logger.Debug("Entering Map View Port Image Loader Thread");

            try
            {
                while (WaitHandle.Wait(0) == false)
                {
                    var isUpdated = viewModel.UpdateImages(Images, CreateImage, MergeImages);

                    if (isUpdated == false)
                    {
                        WaitHandle.Wait(100);
                    }
                    else
                    {
                        RefreshMapAsync();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.ErrorException(e, "Unexpected Error in Map View Port Image Loader Thread");
            }

            Logger.Debug("Exiting Map View Port Image Loader Thread");
        }

        protected virtual Image CreateImage(Uri uri)
        {
            Image image = null;

            if (uri.IsFile)
            {
                image = CreateFileImage(uri);
            }
            else if (uri.Scheme == CacheTileStorage.Scheme)
            {
                image = CreateCacheImage(uri);
            }
            else
            {
                Logger.Warn("Unrecognized Uri Scheme: {0}", uri);
            }

            return image;
        }

        private Image CreateFileImage(Uri uri)
        {
            Image image = null;

            try
            {
                image = new Bitmap(uri.LocalPath);
            }
            catch (Exception e)
            {
                if (e is ArgumentException || e is IOException)
                {
                    // there is a very small chance that the file is being written to as we try and load it
                    // we can ignore this error;

                    Logger.Warn(e.Message);
                }
                else
                {
                    throw;
                }
            }

            return image;
        }

        private Image CreateCacheImage(Uri uri)
        {
            Image image = null;

            var content = Storage.GetTileImage(uri);

            if (content != null && content.Length > 0)
            {
                using (var ms = new MemoryStream(content))
                {
                    image = new Bitmap(ms);
                }
            }

            return image;
        }

        private Image MergeImages(Image baseImage, Image overlayImage)
        {
            Image result = baseImage;

            if (result == null)
            {
                result = overlayImage;
            }
            else if (overlayImage != null)
            {
                using (var g = Graphics.FromImage(result))
                {
#if PocketPC
                    // this code is required to enable transparency drawing in CF
                    // this will make all white pixels transparent (PNG transparency is interpreted as white on CF)

                    var attr = new System.Drawing.Imaging.ImageAttributes();

                    attr.SetColorKey(Color.White, Color.White);

                    g.DrawImage(overlayImage, new Rectangle(0, 0, result.Width, result.Height), 0, 0, result.Width, result.Height, GraphicsUnit.Pixel, attr);
#else
                    g.DrawImage(overlayImage, 0, 0);
#endif
                }

                overlayImage.Dispose();
            }

            return result;
        }

        protected virtual WinFormsLayer GetLayer(LayerViewModel viewModel)
        {
            WinFormsLayer layer = null;

            Layers.TryGetValue(viewModel.Guid, out layer);

            return layer;
        }

        public virtual void AddLayer(WinFormsLayer layer)
        {
            Layers.Add(layer.ViewModel.Guid, layer);
        }

        #region Mouse Handling

        private Point DragOrigin { get; set; }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                DragOrigin = new Point(e.X, e.Y);
            }
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            DragOrigin = Point.Empty;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (DragOrigin.IsEmpty == false)
            {
                var x = DragOrigin.X - e.X;
                var y = DragOrigin.Y - e.Y;

                ViewModel.Pan(x, y);

                DragOrigin = new Point(e.X, e.Y);
            }
        }

        private void OnDoubleClick(object sender, MouseEventArgs e)
        {
            if (ViewModel.Map.ZoomIn.CanExecute())
            {
                ViewModel.Map.CenterPosition = ViewModel.GetLocalPosition(e.X, e.Y);

                ViewModel.Map.ZoomIn.Execute();
            }
        }

#if !PocketPC
        private void OnMouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (ViewModel.Map.ZoomIn.CanExecute())
                {
                    ViewModel.Map.ZoomIn.Execute();
                }
            }
            else if (e.Delta < 0)
            {
                if (ViewModel.Map.ZoomOut.CanExecute())
                {
                    ViewModel.Map.ZoomOut.Execute();
                }
            }
        }
#endif

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            WaitHandle.Set();

            foreach (var layer in Layers.Values)
            {
                layer.Dispose();
            }

            // small delay to wait for threads to cleanup
            new ManualResetEventSlim(false).Wait(1000);
        }

        #endregion
    }
}
