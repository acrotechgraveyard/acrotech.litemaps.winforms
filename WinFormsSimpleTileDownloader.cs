﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Acrotech.LiteMaps.Engine.Storage;

namespace Acrotech.LiteMaps.WinForms
{
    public class WinFormsSimpleTileDownloader : SimpleTileDownloader
    {
        protected override WebRequest CreateWebRequest(Uri uri)
        {
            var req = base.CreateWebRequest(uri);

            if (req is HttpWebRequest)
            {
                ((HttpWebRequest)req).UserAgent = SimpleTileDownloader.UserAgent;
            }

            return req;
        }
    }
}
