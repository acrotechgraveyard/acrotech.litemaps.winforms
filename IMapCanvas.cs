﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Acrotech.LiteMaps.WinForms
{
    public interface IMapCanvas
    {
#if PocketPC
        event EventHandler Resize;
#else
        event EventHandler SizeChanged;
        event MouseEventHandler MouseWheel;
#endif
        event PaintEventHandler Paint;
        event MouseEventHandler MouseDown;
        event MouseEventHandler MouseUp;
        event MouseEventHandler MouseMove;
        event MouseEventHandler MouseDoubleClick;

        Rectangle Bounds { get; }

        void Invalidate();
        object Invoke(Delegate action);
    }
}
