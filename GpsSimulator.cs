﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using DotSpatial.Positioning;
using Acrotech.PortableLogAdapter;
using Acrotech.LiteMaps.Engine;

namespace Acrotech.LiteMaps.WinForms
{
    public class GpsSimulator : IDisposable
    {
        private static readonly ILogger Logger = Services.LogManager.GetLogger<GpsSimulator>();

        public static readonly GpsSimulator Default = new GpsSimulator();

        private readonly ManualResetEventSlim WaitHandle = new ManualResetEventSlim(false);
        private readonly Random rng = new Random();

        public event Action<Position, Distance> PositionChanged = null;

        public GpsSimulator()
            : this(new Position(new Latitude(53.3041647), new Longitude(-113.5226291)), Distance.FromMeters(50), 0, 0.25, 0, 3, 0, 5)
        {
        }

        public GpsSimulator(Position initialPosition, Distance initialAccuracy, double minDirectionChange, double maxDirectionChange, double minDistanceChange, double maxDistanceChange, double minAccuracyChange, double maxAccuracyChange)
        {
            CurrentPosition = initialPosition;
            CurrentAccuracy = initialAccuracy;
            CurrentDirection = Azimuth.North;

            MinDirectionChange = minDirectionChange;
            MaxDirectionChange = maxDirectionChange;
            MinDistanceChange = minDistanceChange;
            MaxDistanceChange = maxDistanceChange;
            MinAccuracyChange = minAccuracyChange;
            MaxAccuracyChange = maxAccuracyChange;
        }

        public Position CurrentPosition { get; private set; }
        public Distance CurrentAccuracy { get; private set; }

        private double MinDirectionChange { get; set; }
        private double MaxDirectionChange { get; set; }
        private double MinDistanceChange { get; set; }
        private double MaxDistanceChange { get; set; }
        private double MinAccuracyChange { get; set; }
        private double MaxAccuracyChange { get; set; }

        private Azimuth CurrentDirection { get; set; }

        public void Start()
        {
            WaitHandle.Reset();

            ThreadPool.QueueUserWorkItem(_ => Simulate());
        }

        private void Simulate()
        {
            Logger.Debug("Entering Gps Simulate Thread");

            try
            {
                while (WaitHandle.Wait(1000) == false)
                {
                    OnPositionChanged(NextPosition(), NextAccuracy());
                }
            }
            catch (Exception e)
            {
                Logger.ErrorException(e, "Unexpected Error in Gps Simulate Thread");
            }

            Logger.Debug("Exiting Gps Simulate Thread");
        }

        protected virtual void OnPositionChanged(Position position, Distance accuracy)
        {
            var positionChanged = PositionChanged;

            if (positionChanged != null)
            {
                positionChanged(position, accuracy);
            }
        }

        private Position NextPosition()
        {
            var dDirection = (MinDirectionChange + rng.NextDouble() * (MaxDirectionChange - MinDirectionChange)) * (rng.Next(2) - 1);
            var dDistance = MinDistanceChange + rng.NextDouble() * (MaxDistanceChange - MinDistanceChange);

            CurrentDirection = Azimuth.FromRadians(CurrentDirection.ToRadians().Add(dDirection));

            CurrentPosition = CurrentPosition.TranslateTo(CurrentDirection, Distance.FromMeters(dDistance));

            //Console.WriteLine("{0:0.00} m @ {1:0.00} ({2:0.00})", dDistance, dDirection, CurrentDirection.DecimalDegrees);

            return CurrentPosition;
        }

        private Distance NextAccuracy()
        {
            var dAccuracy = MinAccuracyChange + (rng.NextDouble() - 0.5) * (MaxAccuracyChange - MinAccuracyChange);

            CurrentAccuracy = CurrentAccuracy.Add(dAccuracy);

            if (CurrentAccuracy.ToMeters().Value < 0)
            {
                CurrentAccuracy = Distance.Empty;
            }

            //Console.WriteLine("{0:0.00} m ({1:0.00 u})", dAccuracy, CurrentAccuracy.ToMeters());

            return CurrentAccuracy;
        }

        #region IDisposable Members

        public void Dispose()
        {
            WaitHandle.Set();
        }

        #endregion
    }
}
