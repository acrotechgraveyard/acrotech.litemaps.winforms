﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.Engine.Storage;
using Acrotech.LiteMaps.Storage;
using Acrotech.PortableIoCAdapter;
using Acrotech.PortableIoCAdapter.Containers;
using Acrotech.PortableLogAdapter;
using Acrotech.PortableLogAdapter.NLogAdapter;

namespace Acrotech.LiteMaps.WinForms
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Initialize();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }

        public static PortableIoCAdapter.IContainer Container { get; private set; }
        public static ILogManager LogManager { get; private set; }

        public static bool IsInDesignMode
        {
            get { return LicenseManager.UsageMode == LicenseUsageMode.Designtime || Debugger.IsAttached == true; }
        }

        private static void Initialize()
        {
            Container = new SimpleContainer();
            LogManager = NLogManager.Default;

            Container.Register<ILogManager>(LogManager);
            Container.Register<ITileStorage>(() => new LocalTileStorage(Container));
            Container.Register<ITileDownloader>(() => new WinFormsSimpleTileDownloader());

            Services.Initialize(Container);
        }

        public static T DesignModeSafe<T>(Func<T> action, T defaultValue = default(T))
        {
            T value = defaultValue;

            if (IsInDesignMode == false)
            {
                value = action();
            }

            return value;
        }
    }
}
