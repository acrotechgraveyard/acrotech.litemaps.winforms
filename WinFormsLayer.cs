﻿using Acrotech.LiteMaps.Engine;
using System.Drawing;
using System;

namespace Acrotech.LiteMaps.WinForms
{
    public abstract class WinFormsLayer : IDisposable
    {
        public WinFormsLayer(LayerViewModel viewModel)
        {
            ViewModel = viewModel;
        }

        public LayerViewModel ViewModel { get; private set; }

        public virtual void OnRender(Graphics g, ViewPortViewModel viewModel, MapViewPort viewPort)
        {
            // do nothing
        }

        #region IDisposable Members

        public virtual void Dispose()
        {
            // do nothing
        }

        #endregion
    }
}
