﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.Engine.Sources;
using Acrotech.LiteMaps.Engine.Storage;
using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WinForms.Layers
{
    public class TileDownloadStatusLayer : UpdatingTextLayer
    {
        public const string Name = "Tile Download Status";

        private static readonly ILogger Logger = Services.LogManager.GetLogger<TileDownloadStatusLayer>();

        public TileDownloadStatusLayer(MapViewPort viewPort, LayerCornerPosition position)
            : base(Name, viewPort, position)
        {
        }

        public TileDownloadStatusLayer(MapViewPort viewPort)
            : this(viewPort, default(LayerCornerPosition))
        {
        }

        public ITileDownloader Downloader { get { return TileImageSource.Downloader; } }

        protected override string Text { get { return Downloader.Count.ToString(); } }
    }
}
