﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Acrotech.LiteMaps.Engine;
using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WinForms.Layers
{
    public abstract class UpdatingTextLayer : UpdatingLayer
    {
        private static readonly ILogger Logger = Services.LogManager.GetLogger<UpdatingTextLayer>();

        public UpdatingTextLayer(string name, MapViewPort viewPort, LayerCornerPosition position)
            : base(name, viewPort, position)
        {
        }

        public UpdatingTextLayer(string name, MapViewPort viewPort)
            : base(name, viewPort)
        {
        }

        protected virtual string Text { get { return string.Empty; } }

        public override void OnRender(Graphics g, ViewPortViewModel viewModel, MapViewPort viewPort)
        {
            base.OnRender(g, viewModel, viewPort);

            if (ViewModel.IsEnabled)
            {
                var txt = Text;

                if (string.IsNullOrEmpty(txt) == false)
                {
                    var bounds = GetBounds(g, txt, viewPort);

#if !PocketPC
                    g.FillRectangle(MapViewPort.DebugTextBackgroundBrush, bounds);
#endif

                    g.DrawString(txt, MapViewPort.DebugFont, MapViewPort.DebugTextBrush, bounds.X, bounds.Y);
                }
            }
        }
    }
}
