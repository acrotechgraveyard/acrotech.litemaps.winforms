﻿using System.Globalization;
using System.Threading;
using System.Windows;
using Acrotech.LiteMaps.Engine;
using System.Drawing;
using System;
using DotSpatial.Positioning;

namespace Acrotech.LiteMaps.WinForms.Layers
{
    public class TargetMarkerLayer : WinFormsLayer
    {
        public const string Name = "Target";

        public TargetMarkerLayer()
            : this(Color.Blue, 10)
        {
        }

        public TargetMarkerLayer(Color color, int size)
            : base(new LayerViewModel(Name))
        {
            Brush = new SolidBrush(color);
            Pen = new Pen(color, (int)Math.Ceiling(size / 10f));
            Size = size;

            ViewModel.IsEnabled = true;
        }

        private Brush Brush { get; set; }
        private Pen Pen { get; set; }
        private int Size { get; set; }

        public override void OnRender(Graphics g, ViewPortViewModel viewModel, MapViewPort viewPort)
        {
            base.OnRender(g, viewModel, viewPort);

            var p = viewModel.Map.TargetPosition;
            var a = viewModel.Map.TargetAccuracy;

            if (p.IsEmpty == false && p.IsInvalid == false)
            {
                int x, y;

                viewModel.GetLocalXY(p, out x, out y);

                var tri = new[] { new Point(x, y), new Point(x - Size, y - Size * 3), new Point(x + Size, y - Size * 3) };

                g.FillPolygon(Brush, tri);
                g.FillEllipse(Brush, x - Size, y - Size * 4, Size * 2, Size * 2);

                if (a.IsEmpty == false && a.IsInvalid == false)
                {
                    int xp, yp;
                    var pp = p.TranslateTo(Azimuth.East, a);

                    viewModel.GetLocalXY(pp, out xp, out yp);

                    var r = Math.Abs(x - xp);

                    if (r > Size)
                    {
                        g.DrawEllipse(Pen, x - r, y - r, r * 2, r * 2);
                    }
                }
            }
        }
    }
}
