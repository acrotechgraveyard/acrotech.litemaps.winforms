﻿using System.Globalization;
using System.Threading;
using System.Windows;
using Acrotech.LiteMaps.Engine;
using System.Drawing;
using System;
using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WinForms.Layers
{
    public class FPSLayer : UpdatingTextLayer
    {
        public const string Name = "FPS";

        private static readonly ILogger Logger = Services.LogManager.GetLogger<FPSLayer>();

        public FPSLayer(MapViewPort viewPort, LayerCornerPosition position)
            : base(Name, viewPort, position)
        {
        }

        public FPSLayer(MapViewPort viewPort)
            : this(viewPort, default(LayerCornerPosition))
        {
        }

        public int Counter { get; private set; }
        public int FPS { get; private set; }

        protected override string Text { get { return string.Format("{0} FPS", FPS); } }

        protected override void OnUpdate()
        {
            base.OnUpdate();

            FPS = Counter;
            Counter = 0;
        }
    }
}
