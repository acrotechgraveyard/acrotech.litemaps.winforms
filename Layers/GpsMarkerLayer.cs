﻿using System.Globalization;
using System.Threading;
using System.Windows;
using Acrotech.LiteMaps.Engine;
using System.Drawing;
using System;
using DotSpatial.Positioning;

#if PocketPC
using Acrotech.LiteMaps.Compat;
#endif

namespace Acrotech.LiteMaps.WinForms.Layers
{
    public class GpsMarkerLayer : WinFormsLayer
    {
        public const string Name = "GPS";

        public GpsMarkerLayer()
            : this(2, 2, Color.Red)
        {
        }

        public GpsMarkerLayer(float pinRadius, float ringStroke, Color color)
            : base(new LayerViewModel(Name))
        {
            PinRadius = pinRadius;
            Brush = new SolidBrush(color);
            Pen = new Pen(color, ringStroke);

            ViewModel.IsEnabled = true;
        }

        private float PinRadius { get; set; }
        private Brush Brush { get; set; }
        private Pen Pen { get; set; }

        public override void OnRender(Graphics g, ViewPortViewModel viewModel, MapViewPort viewPort)
        {
            base.OnRender(g, viewModel, viewPort);

            var p = viewModel.Map.GpsPosition;
            var a = viewModel.Map.GpsAccuracy;

            if (p.IsEmpty == false && p.IsInvalid == false)
            {
                int x, y;

                viewModel.GetLocalXY(p, out x, out y);
                
                g.FillEllipse(Brush, x - PinRadius, y - PinRadius, PinRadius * 2, PinRadius * 2);

                if (a.IsEmpty == false && a.IsInvalid == false)
                {
                    int xp, yp;
                    var pp = p.TranslateTo(Azimuth.East, a);

                    viewModel.GetLocalXY(pp, out xp, out yp);

                    var r = Math.Abs(x - xp);

                    g.DrawEllipse(Pen, x - r, y - r, r * 2, r * 2);
                }
            }
        }
    }
}
