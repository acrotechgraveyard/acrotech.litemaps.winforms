﻿using System.Globalization;
using System.Threading;
using System.Windows;
using Acrotech.LiteMaps.Engine;
using System.Drawing;
using System;
using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WinForms.Layers
{
    public abstract class UpdatingLayer : WinFormsLayer
    {
        private static readonly ILogger Logger = Services.LogManager.GetLogger<UpdatingLayer>();

        public UpdatingLayer(string name, MapViewPort viewPort, LayerCornerPosition position)
            : base(new LayerViewModel(name))
        {
            ViewPort = viewPort;
            Position = position;

            ViewModel.IsEnabled = true;

            WaitHandle = new ManualResetEventSlim(false);

            ThreadPool.QueueUserWorkItem(_ => Updater());
        }

        public UpdatingLayer(string name, MapViewPort viewPort)
            : this(name, viewPort, default(LayerCornerPosition))
        {
        }

        public MapViewPort ViewPort { get; private set; }
        public LayerCornerPosition Position { get; set; }

        protected virtual int Timeout { get { return 1000; } }

        private ManualResetEventSlim WaitHandle { get; set; }

        private void Updater()
        {
            Logger.Debug("Entering Updating Text Layer Updater Thread");

            try
            {
                while (WaitHandle.Wait(Timeout) == false)
                {
                    OnUpdate();

                    ViewPort.RefreshMapAsync();
                }
            }
            catch (Exception e)
            {
                Logger.ErrorException(e, "Unexpected Error in Updating Text Layer Updater Thread");
            }

            Logger.Debug("Exiting Updating Text Layer Updater Thread");
        }

        protected virtual void OnUpdate()
        {
            // do nothing
        }

        protected Rectangle GetBounds(Graphics g, string txt, MapViewPort viewPort)
        {
            var bounds = Rectangle.Empty;

#if PocketPC
            if (Position == LayerCornerPosition.TopLeft)
            {
                bounds = new Rectangle(0, 0, 0, 0);
            }
            else
#endif
            {
                var size = g.MeasureString(txt, MapViewPort.DebugFont);
                var width = (int)Math.Ceiling(size.Width);
                var height = (int)Math.Ceiling(size.Height);

                switch (Position)
                {
                    case LayerCornerPosition.TopLeft:
                        bounds = new Rectangle(0, 0, width, height);
                        break;
                    case LayerCornerPosition.TopRight:
                        bounds = new Rectangle(viewPort.ViewModel.ViewPortWidth - width, 0, width, height);
                        break;
                    case LayerCornerPosition.BottomRight:
                        bounds = new Rectangle(viewPort.ViewModel.ViewPortWidth - width, viewPort.ViewModel.ViewPortHeight - height, width, height);
                        break;
                    case LayerCornerPosition.BottomLeft:
                        bounds = new Rectangle(0, viewPort.ViewModel.ViewPortHeight - height, width, height);
                        break;
                }
            }

            return bounds;
        }

        public override void Dispose()
        {
            WaitHandle.Set();
        }
    }

    public enum LayerCornerPosition
    {
        TopLeft,
        TopRight,
        BottomRight,
        BottomLeft
    }
}
